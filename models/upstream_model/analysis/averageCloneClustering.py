from essentials import *
import os

def plot_clustering_per_week(cloneClusteringPerWeek, cloneType, c):
    avgClusteringPerWeek = []
    std = []
    numberOfweeks = len(cloneClusteringPerWeek)
    weeks = range(numberOfweeks)

    for week in weeks:
        avgClusteringPerWeek.append(np.array(cloneClusteringPerWeek[week]).mean())
        std.append(np.array(cloneClusteringPerWeek[week]).std())
        # avgClusteringPerWeek.append(cloneClusteringPerWeek[week] / totalNumOfClonesPerWeek[week])

    # plt.errorbar(weeks, avgClusteringPerWeek, std, linestyle='None', marker='o')

    d = {
        'data': {
            'x': {cloneType: weeks},
            'y': {
                cloneType: (avgClusteringPerWeek, 'k--', std, '#1B2ACC', '#089FFF'),
            },
        },
        'xlabel': 'weeks',
        'ylabel': 'avg clustering coefficient',
        'title': "Clone clustering over time",
        'savefig': c['analysis_output'] + "clone_clustering_std.png"
    }

    plot(d)

    # re-plot without standard deviation
    d['data']['y'] = {
        cloneType: (avgClusteringPerWeek, 'k--', [], '', ''),
    }
    d['savefig'] = c['analysis_output'] + "clone_clustering.png"

    plot(d)

def avgCloneClustering(c, options):

    if c['induction_level'] > 0:
        MutCloneClusteringPerWeek = {}
        WTcloneClusteringPerWeek = {}

        if options['useVars']:
            MutCloneClusteringPerWeek  = readVariableFromDisk('MutCloneClusteringPerWeek', c)
            WTcloneClusteringPerWeek = readVariableFromDisk('WTcloneClusteringPerWeek', c)

        else:
            for filename in os.listdir(c['netlogo_output']):
                [week, seed] = re.findall(r"[-]?\d+|\d+", filename)

                week = int(week)
                agents = parse_netlogo_world(c['netlogo_output'] + filename)
                clones = get_clones(agents)  # group agents by clone ID
                mutantClones, wtClones = split_clones(clones)

                MutCloneClusteringPerWeek.setdefault(week, [])
                WTcloneClusteringPerWeek.setdefault(week, [])

                for cloneID, cloneAgents in wtClones.items():
                    cloneGraph = get_clone_graph(cloneAgents)
                    WTcloneClusteringPerWeek[week].append(nx.average_clustering(cloneGraph))

                for cloneID, cloneAgents in mutantClones.items():
                    cloneGraph = get_clone_graph(cloneAgents)
                    MutCloneClusteringPerWeek[week].append(nx.average_clustering(cloneGraph))

        if options['dumpVars']:
            writeVariableToDisk(WTcloneClusteringPerWeek, 'WTcloneClusteringPerWeek', c)
            writeVariableToDisk(MutCloneClusteringPerWeek, 'WTcloneClusteringPerWeek', c)

        plot_clustering_per_week(WTcloneClusteringPerWeek, "WT", c)
        plot_clustering_per_week(MutCloneClusteringPerWeek, "MUT", c)

    else:
        cloneClusteringPerWeek = {}

        if options['useVars']:
            cloneClusteringPerWeek = readVariableFromDisk('cloneClusteringPerWeek', c)
        else:
            for filename in os.listdir(c['netlogo_output']):
                [week, seed] = re.findall(r"[-]?\d+|\d+", filename)

                week = int(week)
                agents = parse_netlogo_world(c['netlogo_output'] + filename)
                clones = get_clones(agents)  # group agents by clone ID

                cloneClusteringPerWeek.setdefault(week, [])

                for cloneID, cloneAgents in clones.items():
                    cloneGraph = get_clone_graph(cloneAgents)
                    cloneClusteringPerWeek[week].append(nx.average_clustering(cloneGraph))

        if options['dumpVars']:
            writeVariableToDisk(cloneClusteringPerWeek, 'cloneClusteringPerWeek', c)

        plot_clustering_per_week(cloneClusteringPerWeek, "WT", c)
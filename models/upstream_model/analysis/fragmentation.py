from essentials import *
import os

def plot_fragmentation_per_week(totalNumOfClonesPerWeek, fragmentedClonesPerWeek, cloneType, c):
    numberOfweeks = len(totalNumOfClonesPerWeek)
    fragmentationPerWeek = []

    weeks = range(numberOfweeks)

    for week in weeks:
        fragmentationPerWeek.append(fragmentedClonesPerWeek[week] / totalNumOfClonesPerWeek[week])

    d = {
        'data': {
            'x': {cloneType: weeks},
            'y': {
                cloneType: (fragmentationPerWeek, 'k--', [], '', ''),
            },
        },
        'xlabel': 'weeks',
        'ylabel': 'fragmented clones',
        'title': "Clone fragmentation over time",
        'savefig': c['analysis_output'] + "clone_fragmentation.png"
    }

    plot(d)

def fragmentation(c, options):
    fragmentedClonesPerWeek = {}
    totalNumOfClonesPerWeek = {}

    if options['useVars']:
        fragmentedClonesPerWeek = readVariableFromDisk('fragmentedClonesPerWeek', c)
    else:
        for filename in os.listdir(c['netlogo_output']):
            [week, seed] = re.findall(r"[-]?\d+|\d+", filename)

            week = int(week)
            agents = parse_netlogo_world(c['netlogo_output'] + filename)
            clones = get_clones(agents)  # group agents by clone ID

            totalNumOfClonesPerWeek.setdefault(week, 0)
            totalNumOfClonesPerWeek[week] += len(clones)

            fragmentedClonesPerWeek.setdefault(week, [])

            for cloneID, cloneAgents in clones.items():
                cloneGraph = get_clone_graph(cloneAgents)

                if is_fragmented(cloneGraph):
                    fragmentedClonesPerWeek[week] += 1

    if options['dumpVars']:
        writeVariableToDisk(fragmentedClonesPerWeek, 'fragmentedClonesPerWeek', c)

    plot_fragmentation_per_week(totalNumOfClonesPerWeek,fragmentedClonesPerWeek, "WT", c)
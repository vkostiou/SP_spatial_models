import networkx as nx

class Clone:
    def __init__(self, cloneID, members ):
        self.cloneID = cloneID
        self.members = members
        self.size = len(members)
        self.shape = None
        self.graph = nx.Graph(self.get_adjacencies())
        self.avg_clustering_coefficient = nx.average_clustering(self.graph)

    def get_adjacencies(self):
        adjacencies = {}
        for cm1 in self.members:
            adjacencies.setdefault(cm1, [])
            for cm2 in self.members:
                if cm2.turtleID in cm1.sixNeighbors:
                    adjacencies[cm1].append(cm2)

        return adjacencies

    def is_fragmented(self):
        return not nx.is_connected(self.graph)


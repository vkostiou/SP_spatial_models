from essentials import *
import os

def getStd(data):
    std = []

    for week in data.keys():
        std.append(np.array(data[week]).std())

    return std

expMutPercentageData = {
    '1.5': [0.9, 0.7, 0.6, 0.0],
    '3': [1.2, 0.7, 0.8, 2.3],
    '6': [0.7, 3.0, 3.0],
    '12': [1.3, 4.1, 0.7, 4.5],
    '24': [5.2, 2.5, 3.2, 10.5],
    '52': [18.2, 13.7, 18.0]

}

def plot_mutant_percentage_per_week(mutantPercentagePerWeek, c):
    numberOfweeks=len(mutantPercentagePerWeek)
    avgMutantPercentage = []
    std = []

    weeks = range(numberOfweeks)

    for week in weeks:
        avgMutantPercentage.append(np.array(mutantPercentagePerWeek[week]).mean())
        std.append(np.array(mutantPercentagePerWeek[week]).std())

    d = {
        'data': {
            'x': {'CA model': weeks,
                  'experimental': [1.5, 4, 13, 26, 52],
            },
            'y': {
                'CA model': (avgMutantPercentage, 'k--', std, '#1B2ACC', '#089FFF', 'shaded'),
                'experimental': ([2, 5, 38, 78, 100], 'ro', [], '', '', '')
            },
        },
        'xlabel': 'weeks',
        'ylabel': '% Proportion of mutant cells',
        'title': "Tissue take over",
        'savefig': c['analysis_output'] + "mutant_percentage_std.png"
    }

    plot(d)

    # re-plot without standard deviation
    d['data']['y'] = {
        'CA model': (avgMutantPercentage, 'k--', [], '', '', ''),
        'experimental': ([2, 5, 38, 78, 100], 'ro', [], '', '', '')
    }
    d['savefig'] = c['analysis_output'] + "mutant_percentage.png"

    plot(d)

def mutantPercentagePerWeek(c, options):

    mutantPercentagePerWeek = {}

    if options['useVars']:
        mutantPercentagePerWeek = readVariableFromDisk('mutantPercentagePerWeek', c)
    else:
        for filename in os.listdir(c['netlogo_output']):
            [week, seed] = re.findall(r"[-]?\d+|\d+", filename)

            week = int(week)
            agents = parse_netlogo_world(c['netlogo_output'] + filename)

            mutantPercentagePerWeek.setdefault(week, [])
            mutantPercentagePerWeek[week].append(get_mutant_percentage(agents))

    if options['dumpVars']:
        writeVariableToDisk(mutantPercentagePerWeek, 'mutantPercentagePerWeek', c)

    plot_mutant_percentage_per_week(mutantPercentagePerWeek, c)

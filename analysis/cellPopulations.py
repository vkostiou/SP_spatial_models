from essentials import *
import os


def plotCellPopulationsPerWeek(cellPopulations, c):
    numberOfweeks = len(cellPopulations)

    avgAlpha = []
    stdAlpha = []
    avgBeta = []
    stdBeta = []
    avgDouble = []
    stdDouble = []
    avgEmpty = []
    stdEmpty = []

    weeks = range(numberOfweeks)

    for week in weeks:
        avgAlpha.append(np.array(cellPopulations[week]['A']).mean())
        stdAlpha.append(np.array(cellPopulations[week]['A']).std())
        avgBeta.append(np.array(cellPopulations[week]['B']).mean())
        stdBeta.append(np.array(cellPopulations[week]['B']).std())
        avgDouble.append(np.array(cellPopulations[week]['D']).mean())
        stdDouble.append(np.array(cellPopulations[week]['D']).std())
        avgEmpty.append(np.array(cellPopulations[week]['E']).mean())
        stdEmpty.append(np.array(cellPopulations[week]['E']).std())

    dcells = {
        'data': {
            'x': {
                'Proliferating Cells': weeks,
                'Differentiating Cells': weeks
            },
            'y': {
                'Proliferating Cells': (avgAlpha, 'k-o', stdAlpha, 'black', 'black', 'shaded'),
                'Differentiating Cells': (avgBeta, 'r-o', stdBeta, 'red', 'red', 'shaded')
            },
        },
        'xlabel': 'weeks',
        'ylabel': 'number of cells',
        'title': " average population size over time",
        'savefig': c['analysis_output'] + "cell_populations_per_week_std.png"
    }

    plot(dcells)

    # re-plot without standard deviation
    dcells['data']['y'] = {
        'Proliferating Cells': (avgAlpha, 'k-o', [], '', '', ''),
        'Differentiating Cells':(avgBeta, 'r-o', [], '', '', '')
    }
    dcells['savefig'] = c['analysis_output'] + "cell_populations_per_week.png"

    plot(dcells)

    dcrowding = {
        'data': {
            'x': {
                'Doubles': weeks,
                'Empties': weeks
            },
            'y': {
                'Doubles': (avgDouble, 'g-o', stdDouble, 'green', 'green', 'shaded'),
                'Empties': (avgEmpty, 'k-o', stdEmpty, 'black', 'black', 'shaded')
            },
        },
        'xlabel': 'weeks',
        'ylabel': 'number of cells',
        'title': " average crowding / extinction events over time",
        'savefig': c['analysis_output'] + "doubles_empties_per_week_std.png"
    }

    plot(dcrowding)

    # re-plot without standard deviation
    dcrowding['data']['y'] = {
        'Doubles': (avgDouble, 'g-o', [], '', '', ''),
        'Empties':(avgEmpty, 'k-o', [], '', '', '')
    }
    dcrowding['savefig'] = c['analysis_output'] + "doubles_empties_per_week.png"

    plot(dcrowding)

    # weeks = range(numberOfweeks)
    # plt.plot(weeks, avgAlpha, 'r--', label="Proliferating cells")
    # plt.plot(weeks, avgBeta, 'b--', label="Differentiating cells")
    # plt.ylabel('average population size')
    # plt.xlabel('weeks')
    # plt.axis([weeks[0], numberOfweeks, 0, 10000])
    # plt.title("Proliferating and differentiating cells over time")
    # plt.legend()
    # plt.savefig(os.path.dirname(os.path.abspath(__file__))+"/../../analysis_output/populations.png", dpi=300)
    # plt.close()
    # # plt.axis('auto')
    # # plt.show()
    # plt.plot(weeks, avgDouble, 'r--', label="Doubles")
    # plt.plot(weeks, avgEmpty, 'b--', label="Empties")
    # plt.ylabel('average doubles / empties')
    # plt.xlabel('weeks')
    # plt.axis([weeks[0], numberOfweeks, 0, 10000])
    # plt.title("Doubles and empties over time")
    # plt.legend()
    # # plt.show()
    # plt.savefig(os.path.dirname(os.path.abspath(__file__))+"/../../analysis_output/doubles_empties.png", dpi=300)
    # plt.close()



def cellPopulationsPerWeek(c, options):

    cellPopulations = {}

    if options['useVars']:
        cellPopulations = readVariableFromDisk('cellPopulations', c)
    else:
        for filename in os.listdir(c['netlogo_output']):
            [week, seed] = re.findall(r"[-]?\d+|\d+", filename)

            week = int(week)
            agents = parse_netlogo_world(c['netlogo_output'] + filename)

            cellPopulations.setdefault(week, {}).setdefault('A', [])
            cellPopulations[week]['A'].append(get_cell_populations(agents)[0])

            cellPopulations.setdefault(week, {}).setdefault('B', [])
            cellPopulations[week]['B'].append(get_cell_populations(agents)[1])

            cellPopulations.setdefault(week, {}).setdefault('D', [])
            cellPopulations[week]['D'].append(get_cell_populations(agents)[2])

            cellPopulations.setdefault(week, {}).setdefault('E', [])
            cellPopulations[week]['E'].append(get_cell_populations(agents)[3])


    if options['dumpVars']:
        writeVariableToDisk(cellPopulations, 'cellPopulations', c)


    plotCellPopulationsPerWeek(cellPopulations, c)
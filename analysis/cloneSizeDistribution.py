from essentials import *
import os

def plot_distributions(data, yaxis, c):
    df = pd.DataFrame(data)

    box = sns.boxplot(x='week', y=yaxis, data=df)
    # giving title to the plot
    plt.title('Graph')
    # plt.show()
    plt.savefig(c['analysis_output'] + "boxplot_"+yaxis+".png", dpi=300)
    plt.close()

    # swarm = sns.swarmplot(x='week', y=yaxis, data=df, size=1);
    # plt.title('Graph')
    # # plt.show()
    # plt.savefig(c['analysis_output'] + "swarmplot_"+yaxis+".png", dpi=300)
    # plt.close()

def plotCloneSurvivalPerWeek(numOfClonesPerWeek, cloneType, c):
    numberOfweeks = len(numOfClonesPerWeek)
    avgCloneSurvPerWeek = []
    std = []

    weeks = range(numberOfweeks)

    for week in weeks:
        avgCloneSurvPerWeek.append(np.array(numOfClonesPerWeek[week]).mean())
        std.append(np.array(numOfClonesPerWeek[week]).std())

    d = {
        'data': {
            'x': {
                cloneType + ' model': weeks,
            },
            'y': {
                cloneType + ' model': (avgCloneSurvPerWeek, 'b-', std, '#1B2ACC', '#089FFF', 'shaded'),
            },
        },
        'xlabel': 'weeks',
        'ylabel': 'Number of clones',
        'title': cloneType +" surviving clones over time",
        'savefig': c['analysis_output'] + cloneType + "average_clone_surv_per_week_std.png"
    }

    plot(d)

def cloneSizeDistributionPerWeek (c, options):

    if c['induction_level'] > 0:

        mutCloneSizes = []
        wtCloneSizes = []
        MUTnumberOfClones = {}
        WTnumberOfClones = {}

        if options['useVars']:
            mutCloneSizes = readVariableFromDisk('mutCloneSizes', c)
            wtCloneSizes = readVariableFromDisk('wtCloneSizes', c)
            MUTnumberOfClones = readVariableFromDisk('MUTnumberOfClones', c)
            WTnumberOfClones = readVariableFromDisk('WTnumberOfClones', c)
        else:
            for filename in os.listdir(c['netlogo_output']):
                [week, seed] = re.findall(r"[-]?\d+|\d+", filename)

                week = int(week)
                agents = parse_netlogo_world(c['netlogo_output'] + filename)
                clones = get_clones(agents)  # group agents by clone ID
                mutantClones, wtClones = split_clones(clones)

                MUTnumberOfClones.setdefault(week, [])
                WTnumberOfClones.setdefault(week, [])
                MUTnumberOfClones[week].append(len(mutantClones))
                WTnumberOfClones[week].append(len(wtClones))

                for cloneID, cloneAgents in wtClones.items():
                    wtCloneSizes.append({"week": week, "wt_clone_size": get_num_of_cells(cloneAgents)})
                for cloneID, cloneAgents in mutantClones.items():
                    mutCloneSizes.append({"week": week, "mutant_clone_size": get_num_of_cells(cloneAgents)})

        if options['dumpVars']:
            writeVariableToDisk(mutCloneSizes, 'mutCloneSizes', c)
            writeVariableToDisk(wtCloneSizes, 'wtCloneSizes', c)
            writeVariableToDisk(MUTnumberOfClones, 'MUTnumberOfClones', c)
            writeVariableToDisk(WTnumberOfClones, 'WTnumberOfClones', c)

        plot_distributions(mutCloneSizes, 'mutant clone size', c)
        plot_distributions(wtCloneSizes, 'WT clone size', c)
        plotCloneSurvivalPerWeek(MUTnumberOfClones, "MUT", c)
        plotCloneSurvivalPerWeek(WTnumberOfClones, "WT", c)


    else:
        cloneSizes = []
        numberOfClones = {}

        if options['useVars']:
            cloneSizes = readVariableFromDisk('cloneSizes', c)
            numberOfClones = readVariableFromDisk('numberOfClones', c)
        else:
            for filename in os.listdir(c['netlogo_output']):
                [week, seed] = re.findall(r"[-]?\d+|\d+", filename)

                week = int(week)
                agents = parse_netlogo_world(c['netlogo_output'] + filename)
                clones = get_clones(agents)  # group agents by clone ID

                numberOfClones.setdefault(week, [])
                numberOfClones[week].append(len(clones))

                if week % 20 == 0 and week !=0:
                    for cloneID, cloneAgents in clones.items():
                        cloneSizes.append({"week": week, "clone_size": get_num_of_cells(cloneAgents)})

        if options['dumpVars']:
            writeVariableToDisk(cloneSizes, 'cloneSizes', c)
            writeVariableToDisk(numberOfClones, 'numberOfClones', c)

        plot_distributions(cloneSizes,'clone_size', c)
        plotCloneSurvivalPerWeek(numberOfClones, "WT", c)

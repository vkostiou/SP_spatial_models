from essentials import *
import os

def getStd(data):
    std = []

    for week in data.keys():
        std.append(np.array(data[week]).std())

    return std

expMutPercentageData = {
    '6': [0.3, 0.7, 1.6, 1.0],
    '12': [0.6, 3.0, 2.2],
    '24': [5.0, 19.0, 9.0],
    '44': [27.0, 15.0, 14.0],
    '67': [36.0, 19.0, 35.0]
}

def plot_mutant_percentage_per_week(mutantPercentagePerWeek, c):
    numberOfweeks=len(mutantPercentagePerWeek)
    avgMutantPercentage = []
    std = []

    weeks = range(numberOfweeks)

    for week in weeks:
        avgMutantPercentage.append(np.array(mutantPercentagePerWeek[week]).mean())
        std.append(np.array(mutantPercentagePerWeek[week]).std())

    d = {
        'data': {
            'x': {'CA model': weeks,
                  'experimental': [6, 12, 24, 44, 67],
            },
            'y': {
                'CA model': (avgMutantPercentage, 'k--', std, '#1B2ACC', '#089FFF', 'shaded'),
                'experimental': ([0.9, 1.9, 11, 18.7, 30], 'ro', getStd(expMutPercentageData), '', '', 'bar')
            },
        },
        'xlabel': 'weeks',
        'ylabel': '% Proportion of mutant cells',
        'title': "Tissue take over",
        'savefig': c['analysis_output'] + "mutant_percentage_std.png"
    }

    plot(d)

    # re-plot without standard deviation
    d['data']['y'] = {
        'CA model': (avgMutantPercentage, 'k--', [], '', '', ''),
        'experimental': ([0.9, 1.9, 11, 18.7, 30], 'ro', getStd(expMutPercentageData), '', '', 'bar')
    }
    d['savefig'] = c['analysis_output'] + "mutant_percentage.png"

    plot(d)

def mutantPercentagePerWeek(c, options):

    mutantPercentagePerWeek = {}

    if options['useVars']:
        mutantPercentagePerWeek = readVariableFromDisk('mutantPercentagePerWeek', c)
    else:
        for filename in os.listdir(c['netlogo_output']):
            [week, seed] = re.findall(r"[-]?\d+|\d+", filename)

            week = int(week)
            agents = parse_netlogo_world(c['netlogo_output'] + filename)

            mutantPercentagePerWeek.setdefault(week, [])
            mutantPercentagePerWeek[week].append(get_mutant_percentage(agents))

    if options['dumpVars']:
        writeVariableToDisk(mutantPercentagePerWeek, 'mutantPercentagePerWeek', c)

    plot_mutant_percentage_per_week(mutantPercentagePerWeek, c)
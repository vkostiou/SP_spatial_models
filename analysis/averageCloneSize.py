from essentials import *
import os

def plotAvgCloneSizePerWeek(totalCloneSizePerWeek, cloneType, c):
    numberOfweeks = len(totalCloneSizePerWeek)
    avgCloneSizesPerWeek = []
    std = []

    weeks = range(numberOfweeks)

    for week in weeks:
        avgCloneSizesPerWeek.append(np.array(totalCloneSizePerWeek[week]).mean())
        std.append(np.array(totalCloneSizePerWeek[week]).std())

    d = {
        'data': {
            'x': {
                cloneType + ' model': weeks,
                'experimental': [1.5,3,6,12]
            },
            'y': {
                cloneType + ' model': (avgCloneSizesPerWeek, 'k--', std, '#1B2ACC', '#089FFF', 'shaded'),
                'experimental': ([2.22,2.28,2.94,6.56], 'ro', [], '', '', '')
            },
        },
        'xlabel': 'weeks',
        'ylabel': 'average clone size',
        'title': cloneType +" average clone size over time",
        'savefig': c['analysis_output'] + cloneType + "average_clone_size_per_week_std.png"
    }

    plot(d)

    # re-plot without standard deviation
    d['data']['y'] = {
        cloneType + ' model': (avgCloneSizesPerWeek, 'k--', [], '', '', ''),
        'experimental':([2.22,2.28,2.94,6.56], 'ro', [], '', '', '')
    }
    d['savefig'] = c['analysis_output'] + cloneType + "average_clone_size_per_week.png"

    plot(d)

# def avgCloneSizePerWeek(c, options):
#     totalCloneSizePerWeek = {}
#     totalMutCloneSizePerWeek = {}
#     totalWTCloneSizePerWeek = {}
#
#     if options['useVars']:
#         if c['induction_level'] > 0:
#             totalMutCloneSizePerWeek = readVariableFromDisk('totalMutCloneSizePerWeek',c)
#             totalWTCloneSizePerWeek = readVariableFromDisk('totalWTCloneSizePerWeek',c)
#         else:
#             totalCloneSizePerWeek = readVariableFromDisk('totalCloneSizePerWeek', c)
#     else:
#         for filename in os.listdir(c['netlogo_output']):
#             [week, seed] = re.findall(r"[-]?\d+|\d+", filename)
#
#             week = int(week)
#             agents = parse_netlogo_world(c['netlogo_output'] + filename)
#             clones = get_clones(agents)  # group agents by clone ID
#
#             totalCloneSizePerWeek.setdefault(week, [])
#             totalMutCloneSizePerWeek.setdefault(week, [])
#             totalWTCloneSizePerWeek.setdefault(week, [])
#
#             if c['induction_level'] > 0:
#                 mutantClones, wtClones = split_clones(clones)
#                 for cloneID, cloneAgents in mutantClones.items():
#                     totalMutCloneSizePerWeek[week].append(get_num_of_cells(cloneAgents))
#                 for cloneID, cloneAgents in wtClones.items():
#                     totalWTCloneSizePerWeek[week].append(get_num_of_cells(cloneAgents))
#             else:
#                 for cloneID, cloneAgents in clones.items():
#                     totalCloneSizePerWeek[week].append(get_num_of_cells(cloneAgents))
#
#     if options['dumpVars']:
#         if c['induction_level'] > 0:
#             writeVariableToDisk(totalMutCloneSizePerWeek,'totalMutCloneSizePerWeek',c)
#             writeVariableToDisk(totalWTCloneSizePerWeek,'totalWTCloneSizePerWeek',c)
#         else:
#             writeVariableToDisk(totalCloneSizePerWeek, 'totalCloneSizePerWeek', c)
#
#     if c['induction_level'] > 0:
#         plotAvgCloneSizePerWeek(totalMutCloneSizePerWeek, "MUT", c)
#         plotAvgCloneSizePerWeek(totalWTCloneSizePerWeek, "WT", c)
#     else:
#         plotAvgCloneSizePerWeek(totalCloneSizePerWeek, "WT", c)

def avgCloneSizePerWeek(c, options):
    if c['induction_level'] > 0:
        totalMutCloneSizePerWeek = {}
        totalWTCloneSizePerWeek = {}

        if options['useVars']:
            totalMutCloneSizePerWeek = readVariableFromDisk('totalMutCloneSizePerWeek', c)
            totalWTCloneSizePerWeek = readVariableFromDisk('totalWTCloneSizePerWeek', c)
        else:
            for filename in os.listdir(c['netlogo_output']):
                [week, seed] = re.findall(r"[-]?\d+|\d+", filename)

                week = int(week)
                agents = parse_netlogo_world(c['netlogo_output'] + filename)
                clones = get_clones(agents)  # group agents by clone ID

                totalMutCloneSizePerWeek.setdefault(week, [])
                totalWTCloneSizePerWeek.setdefault(week, [])

                mutantClones, wtClones = split_clones(clones)
                for cloneID, cloneAgents in mutantClones.items():
                    totalMutCloneSizePerWeek[week].append(get_num_of_cells(cloneAgents))
                for cloneID, cloneAgents in wtClones.items():
                    totalWTCloneSizePerWeek[week].append(get_num_of_cells(cloneAgents))

        if options['dumpVars']:
            writeVariableToDisk(totalMutCloneSizePerWeek, 'totalMutCloneSizePerWeek', c)
            writeVariableToDisk(totalWTCloneSizePerWeek, 'totalWTCloneSizePerWeek', c)

        plotAvgCloneSizePerWeek(totalMutCloneSizePerWeek, "MUT", c)
        plotAvgCloneSizePerWeek(totalWTCloneSizePerWeek, "WT", c)

    else:
        totalCloneSizePerWeek = {}

        if options['useVars']:
            totalCloneSizePerWeek = readVariableFromDisk('totalCloneSizePerWeek', c)
        else:
            for filename in os.listdir(c['netlogo_output']):
                [week, seed] = re.findall(r"[-]?\d+|\d+", filename)

                week = int(week)
                agents = parse_netlogo_world(c['netlogo_output'] + filename)
                clones = get_clones(agents)  # group agents by clone ID

                totalCloneSizePerWeek.setdefault(week, [])
                for cloneID, cloneAgents in clones.items():
                    totalCloneSizePerWeek[week].append(get_num_of_cells(cloneAgents))

        if options['dumpVars']:
            writeVariableToDisk(totalCloneSizePerWeek, 'totalCloneSizePerWeek', c)

        plotAvgCloneSizePerWeek(totalCloneSizePerWeek, "WT", c)
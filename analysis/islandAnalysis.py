from essentials import *
import os

def plotLargestIslandsPerWeek(largestIslands, c):
    numberOfweeks = len(largestIslands)

    avgLargestDoubleIsland = []
    stdLargestDoubleIsland = []
    avgLargestEmptyIsland = []
    stdLargestEmptyIsland = []

    weeks = list(sorted(largestIslands.keys()))

    for week in weeks:
        avgLargestDoubleIsland.append(np.array(largestIslands[week]['double']).mean())
        stdLargestDoubleIsland.append(np.array(largestIslands[week]['double']).std())
        avgLargestEmptyIsland.append(np.array(largestIslands[week]['empty']).mean())
        stdLargestEmptyIsland.append(np.array(largestIslands[week]['empty']).std())

    d = {
        'data': {
            'x': {'largest empty island': weeks,
                  'largest double island': weeks

            },
            'y': {
                'largest empty island': (avgLargestEmptyIsland, 'ko-', stdLargestEmptyIsland, 'black', 'black', 'shaded'),
                'largest double island': (avgLargestDoubleIsland, 'go-', stdLargestDoubleIsland, 'green', 'green', 'shaded')
            },
        },
        'xlabel': 'weeks',
        'ylabel': 'number of lattice cells',
        'title': "island analysis",
        'savefig': c['analysis_output'] + "islands_std.png"
    }

    plot(d)

    # re-plot without standard deviation
    d['data']['y'] = {
        'largest empty island': (avgLargestEmptyIsland, 'ko-', [], '', '', ''),
        'largest double island': (avgLargestDoubleIsland, 'go-', [], '', '', '')
    }
    d['savefig'] = c['analysis_output'] + "islands.png"

    plot(d)


def largestIslandsPerWeek(c, options):

    largestIslands = {}
    #analysis_dir = os.path.dirname(os.path.abspath(__file__))
    #netlogo_output = analysis_dir + "/../../netlogo_output/worlds3/" #c['netlogo_output']

    if options['useVars']:
        largestIslands = readVariableFromDisk('largestIslands', c)
    else:
        for filename in os.listdir(c['netlogo_output']):
            [week, seed] = re.findall(r"[-]?\d+|\d+", filename)

            week = int(week)

            if week == 10 or week == 30 or week == 50 or week == 70:
                agents = parse_netlogo_world(c['netlogo_output'] + filename)

                islandTypes = ["empty", "double"]
                for type in islandTypes:

                    largest_island = []
                    islandAgents = agents[agents["state"] == type]
                    if islandAgents.shape[0] > 0:
                        islandGraph = get_graph(islandAgents)
                        largest_island = max(nx.connected_components(islandGraph), key=len)

                    largestIslands.setdefault(week, {}).setdefault(type, [])
                    largestIslands[week][type].append(len(largest_island))

    if options['dumpVars']:
        writeVariableToDisk(largestIslands, 'largestIslands', c)

    plotLargestIslandsPerWeek(largestIslands, c)

#largestIslandsPerWeek()
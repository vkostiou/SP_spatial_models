from essentials import *
import os

def plotWoundingClosure(woundSizePerWeek, c):
    avgWoundSizePerWeek = []
    std = []

    weeks = list(sorted(woundSizePerWeek.keys()))

    for week in weeks:
        avgWoundSizePerWeek.append(np.array(woundSizePerWeek[week]).mean())
        std.append(np.array(woundSizePerWeek[week]).std())

    d = {
        'data': {
            'x': {'CA model': weeks},
            'y': {
                'CA model': (avgWoundSizePerWeek, 'k-', std, '#1B2ACC', '#089FFF', 'shaded'),
            },
        },
        'xlabel': 'Weeks',
        'ylabel': 'Wound size',
        'title': "Wound size per week",
        'savefig': c['analysis_output'] + "wound_size_std.png"
    }

    plot(d)

    # re-plot without standard deviation
    d['data']['y'] = {
        'CA model': (avgWoundSizePerWeek, 'k-', [], '', '', ''),
    }
    d['savefig'] = c['analysis_output'] + "wound_size.png"

    plot(d)

def plotWoundShapeDistributions(data, yaxis, c):
    plt.rcParams['figure.figsize'] = (12, 4.5)
    df = pd.DataFrame(data)
    ax = sns.swarmplot (x='week', y=yaxis, data=df, size = 5)
    ax = sns.boxplot(x='week', y=yaxis, data=df,
                     showcaps=True, boxprops={'facecolor': 'None'},
                     showfliers=True, whiskerprops={'linewidth': 0})
    #plt.title('Graph')
    ax.set_xlabel("Week", fontsize=14)
    ax.set_ylabel("Boundary ratio", fontsize=14)
    ax.tick_params(labelsize=12)
    plt.tight_layout()
    plt.savefig(c['analysis_output'] + "boxplot_"+yaxis+".png", dpi=300)
    plt.close()

def closureRate(woundSizePerWeek):
    avgSizePerWeek = {}
    lowWeek = -1

    for week, sizeList in woundSizePerWeek.items():
        avgSizePerWeek[week] = (np.array(sizeList).mean())

    weeks = list(sorted(woundSizePerWeek.keys()))

    for week in weeks:
        if avgSizePerWeek[week] <= 0.8:
            lowWeek = week
            break

    rateOfClosure = (avgSizePerWeek[0] - avgSizePerWeek[lowWeek]) / lowWeek

    return rateOfClosure

def shapeOfWounding(agents):

    woundingAgents = agents[agents['state'] == "wounding"]

    if woundingAgents.shape[0] > 1:
        numOfBoundaryAgents = 0

        for index, wa in woundingAgents.iterrows():
            neighbors = get_neighbors(wa, agents)
            if neighbors[neighbors["state"] != 'wounding'].shape[0] > 1:
                numOfBoundaryAgents+=1

        return numOfBoundaryAgents / woundingAgents.shape[0]
    else:
        return 0

def woundingPerWeek(c, options):
    woundSizePerWeek = {}
    boundaryRatios = []
    initialWoundSize = {}

    if options['useVars']:
        woundSizePerWeek = readVariableFromDisk('woundSizePerWeek', c)
        boundaryRatios = readVariableFromDisk('boundaryRatios', c)
    else:
        for filename in os.listdir(c['netlogo_output']):
            [week, seed] = re.findall(r"[-]?\d+|\d+", filename)

            week = int(week)
            agents = parse_netlogo_world(c['netlogo_output'] + filename)

            if week == 0:
                initialWoundSize[seed] = get_num_of_lattice_sites(agents, "wounding")

            woundSizePerWeek.setdefault(week, [])
            woundSizePerWeek[week].append(get_num_of_lattice_sites(agents, "wounding") / initialWoundSize[seed])

            if week % 5 == 0 and week <= 20 :

                boundaryRatios.append({"week": week, "proportion_of_boundary_agents": shapeOfWounding(agents)})

    if options['dumpVars']:
        writeVariableToDisk(woundSizePerWeek, 'woundSizePerWeek', c)
        writeVariableToDisk(boundaryRatios, 'boundaryRatios', c)


    plotWoundingClosure(woundSizePerWeek,c)
    print(closureRate(woundSizePerWeek))
    plotWoundShapeDistributions(boundaryRatios, 'proportion_of_boundary_agents', c)

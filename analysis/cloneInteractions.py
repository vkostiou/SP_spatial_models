from essentials import *
import os
import pprint as pp

def plotMutRatio(interactionRatios,c):
    numberOfweeks = len(interactionRatios)
    avgInteractionRatio = []
    std = []

    weeks = range(numberOfweeks)

    for week in weeks:
        avgInteractionRatio.append(np.array(interactionRatios[week]).mean())
        std.append(np.array(interactionRatios[week]).std())

    d = {
        'data': {
            'x': {
                'ir': weeks,
            },
            'y': {
                'ir': (avgInteractionRatio, 'k--', std, '#1B2ACC', '#089FFF', 'shaded'),
            },
        },
        'xlabel': 'weeks',
        'ylabel': 'mut/total',
        'title': "average mut / total neighbor interactions for mutant clones",
        'savefig': c['analysis_output'] + "average_mut_interactions_std.png"
    }

    plot(d)

    # re-plot without standard deviation
    d['data']['y'] = {
        'ir': (avgInteractionRatio, 'k--', [], '', '', ''),
    }
    d['savefig'] = c['analysis_output'] + "average_mut_interactions.png"

    plot(d)

def getCloneInteractions(c, options):

    cloneInteractions = {}

    if options['useVars']:
        mutCloneInteractionRatiosPerWeek = readVariableFromDisk('mutCloneInteractionRatiosPerWeek', c)

    else:
        #cloneInteractions = {}

        for filename in os.listdir(c['netlogo_output']):
            [week, seed] = re.findall(r"[-]?\d+|\d+", filename)

            week = int(week)
            agents = parse_netlogo_world(c['netlogo_output'] + filename)
            clones = get_clones(agents)  # group agents by clone ID
            mutantClones, wtClones = split_clones(clones)

            for cloneID, cloneAgents in mutantClones.items():
                # print(cloneID)
                seenNeighbors = []
                for index, agent in cloneAgents.iterrows():
                    neighborIDs = agent["six-neighbors"]

                    for nID in neighborIDs:
                        if nID not in seenNeighbors:
                            seenNeighbors.append(nID)
                            neighbor = agents[(agents["who"] == nID) & (agents["cloneid"] != cloneID) & (agents["state"] != 'empty')]

                            if not neighbor.empty:
                                is_mutant = neighbor.loc[nID,'p53-mutation']

                                mutationProfile = 'wt'
                                if is_mutant == 1:
                                    mutationProfile = 'mut'

                                # print(mutantNeighbor.shape[0])
                                neighborCloneID = neighbor.loc[nID,'cloneid']
                                neighborState = neighbor.loc[nID,'state']
                                cloneInteractions.setdefault(week,{}).setdefault(cloneID, {}).setdefault(mutationProfile,{}).setdefault(neighborCloneID,0)
                                if neighborState == 'single':
                                    cloneInteractions[week][cloneID][mutationProfile][neighborCloneID]+=1
                                elif neighborState == 'double':
                                    cloneInteractions[week][cloneID][mutationProfile][neighborCloneID]+=2

            mutCloneInteractionRatiosPerWeek = getInteractionRatios(cloneInteractions)

    if options['dumpVars']:
        writeVariableToDisk(mutCloneInteractionRatiosPerWeek, 'mutCloneInteractionRatiosPerWeek', c)


    plotMutRatio(mutCloneInteractionRatiosPerWeek, c)

def getInteractionRatios(cloneInteractions):

    interactionsPerMutationProfile = {}
    for week in cloneInteractions.keys():
        for cloneID in cloneInteractions[week].keys():
            # print (cloneID)

            for mutationProfile in cloneInteractions[week][cloneID].keys():
                interactionsPerMutationProfile.setdefault(week,{}).setdefault(cloneID, {}).setdefault(mutationProfile, 0)
                for neighborCloneID, interactions in cloneInteractions[week][cloneID][mutationProfile].items():
                    interactionsPerMutationProfile[week][cloneID][mutationProfile]+=interactions

    interactionRatios = {}
    for week in interactionsPerMutationProfile.keys():
        interactionRatios.setdefault(week,[])
        for cloneID in interactionsPerMutationProfile[week].keys():
            mutInteractions = 0
            wtInteractions = 0
            if 'mut' in interactionsPerMutationProfile[week][cloneID]:
                mutInteractions = interactionsPerMutationProfile[week][cloneID]['mut']
            if 'wt' in interactionsPerMutationProfile[week][cloneID]:
                wtInteractions = interactionsPerMutationProfile[week][cloneID]['wt']
            # for mutationProfile, interactions in interactionsPerMutationProfile[week][cloneID].items():
            # mutInteractions = interactionsPerMutationProfile[week][cloneID]['mut']
            interactionRatios[week].append(mutInteractions / (mutInteractions + wtInteractions))

    return interactionRatios






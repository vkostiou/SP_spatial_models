class Agent:
#"who", "xcor", "ycor", "six-neighbors", "cell-type", "state", "time", "cloneid", "creation-time"
    def __init__(self, row, chunkID ):
        self.turtleID = row["who"]
        self.x = row["xcor"]
        self.y = row["ycor"]
        self.cloneID = row["cloneid"]
        self.cellType = row["cell-type"]
        self.state = row["state"]
        self.time = row["time"]
        self.creationTime = row["creation-time"]
        self.sixNeighbors = self.parse_neighbors_string(row["six-neighbors"])
        self.chunkID = chunkID
        #self.immediateNeighbours = immediateNeighbours
        #self.extendedNeighbours = extendedNeighbours

    def parse_neighbors_string(self, string):
        neighbors = string.split(' ')
        neighbors.pop(0)
        neighbors[-1] = neighbors[-1][:-1]
        neighbors = [int(n) for n in neighbors]
        return neighbors

    # def get_turtle_id(self):
    #     return self.turtleID
    #
    # def get_x(self):
    #     return self.x
    #
    # def get_y(self):
    #     return self.y
    #
    # def get_clone_id(self):
    #     return self.cloneID
    #
    # def get_cell_type(self):
    #     return self.cellType
    #
    # def get_state(self):
    #     return self.state
    #
    # def get_time(self):
    #     return self.time
    #
    # def get_creation_time(self):
    #     return self.creationTime

    #def get_immediate_neighbours(self):
    #    return self.immediateNeighbours

    #def get_extended_neighbours(self):
    #    return self.extendedNeighbours

# "who","color","xcor","ycor","immediate-neighbors","extended-neighbors","cell-type","state","time","cloneid","creation-time"
import os
import pprint
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pickle
plt.switch_backend('agg')
from essentials import *
from averageCloneSize import *
from mutantProportion import *
from averageCloneClustering import *
from fragmentation import *
from cloneInteractions import *
from cellDensity import *
from rho import *
from cloneSizeDistribution import *
from cellPopulations import *
from islandAnalysis import *
from woundingAnalysis import *


def main():
    options = parse_command_line_arguments(sys.argv[1:])
    analysisToRun = options['analysisToRun']

    analysis_dir = os.path.dirname(os.path.abspath(__file__))
    netlogo_config = analysis_dir + "/../models/config.nls"
    slurm_config = analysis_dir + "/../../slurm.conf"
    log_file = analysis_dir + "/../../log"
    c = parse_config_files(slurm_config, netlogo_config)
    c['netlogo_output'] = analysis_dir + "/../../netlogo_output/worlds/"
    c['analysis_output'] = analysis_dir + "/../../analysis_output/"

    if 'averageCloneSize' in analysisToRun:
        avgCloneSizePerWeek(c,options)
        log(log_file, 'averageCloneSize')

    if 'cellPopulations' in analysisToRun:
        mutantPercentagePerWeek(c,options)
        log(log_file, 'mutantPercentage')
        cellPopulationsPerWeek(c, options)
        log(log_file, 'cellPopulations')

    if 'cloneClustering' in analysisToRun:
        avgCloneClustering(c, options)
        log(log_file, 'cloneClustering')

    if 'fragmentation' in analysisToRun:
        fragmentation(c, options)
        log(log_file, 'fragmentation')

    if 'cloneInteractions' in analysisToRun:
        getCloneInteractions(c, options)
        log(log_file, 'mutantCloneInteractions')

    if 'cellDensity' in analysisToRun:
        cellDensityPerWeek(c, options)
        log(log_file, 'cellDensity')

    if 'rho' in analysisToRun:
        rhoPerWeek(c, options)
        log(log_file, 'rho')

    if 'cloneSizeDistribution' in analysisToRun:
        cloneSizeDistributionPerWeek(c, options)
        log(log_file, 'cloneSizeDistribution')

    if 'islandAnalysis' in analysisToRun:
        largestIslandsPerWeek(c, options)
        log(log_file, 'largestIslands')

    if 'wounding' in analysisToRun:
        woundingPerWeek(c, options)
        log(log_file, 'wounding')


# def main():
#     analysisToRun = parse_command_line_arguments(sys.argv[1:])
#
#     analysis_dir = os.path.dirname(os.path.abspath(__file__))
#     netlogo_config = analysis_dir + "/../models/config.nls"
#     slurm_config = analysis_dir + "/../../slurm.conf"
#     log_file = analysis_dir + "/../../log"
#     c = parse_config_files(slurm_config, netlogo_config)
#     c['netlogo_output']=analysis_dir + "/../../netlogo_output/worlds/"
#     c['analysis_output']=analysis_dir + "/../../analysis_output/"
#
#     # netlogo_output = "C:\\Users\\Vicky\\Desktop\\hexmodelscluster\\netlogo_output\\mutants\\"
#     #netlogo_output = "C:\\Users\\vk325\\Desktop\\CA_models_Git\\netlogo_output\\worlds\\tmp\\"
#     #netlogo_output = "C:\\Users\\Vicky\\Desktop\\hexmodelscluster\\netlogo_output\\worlds\\"
#     #netlogo_output = "C:\\Users\\Vicky\\Desktop\\hexmodelscluster\\outputs2\\"
#
#     numberOfweeks = c["weeks"] + 1
#     numOfSims = c["numOfSims"]
#     induction_level = c["induction"]
#
#     cellPopulations = {}
#     totalNumOfClonesPerWeek = {}
#
#
#     fragmentedClonesPerWeek = {}
#     cloneClusteringPerWeek = {}
#
#     chunkSizePerWeek = []
#     localRho = []
#     cloneSize = []
#     mutCloneSize = []
#     wtCloneSize = []
#
#     mutantPercentagePerWeek={}
#
#     # read netlogo output file by file (each file corresponds to one week and one seed)
#     for filename in os.listdir(netlogo_output):
#         [week, seed] = re.findall(r"[-]?\d+|\d+", filename)
#
#         week = int(week)
#         seed = int(seed)
#
#         agents = parse_netlogo_world(netlogo_output + filename)
#         clones = get_clones(agents)  # group agents by clone ID
#         if induction_level > 0:
#             mutantClones, wtClones = split_clones(clones)
#
#         # the below analyses are performed every 20 weeks
#         if week % 20 == 0 and week != 0:
#             if 'gridSegregation' in analysisToRun:
#                 chunks = get_grid_chunks(agents)  # split grid to smaller sections
#                 for chunk in chunks:
#                     # count total number of cells per chunk
#                     chunkSizePerWeek.append({"week": week, "chunk_size": get_num_of_cells(chunk)})
#
#                     # calculate local rho
#                     localRho.append({"week": week, "local_rho": get_local_rho(chunk)})
#
#             if 'cloneSizeDistribution' in analysisToRun:
#                 if induction_level > 0:
#                     for cloneID, cloneAgents in wtClones.items():
#                         wtCloneSize.append({"week": week, "wt_clone_size": get_num_of_cells(cloneAgents)})
#
#                     for cloneID, cloneAgents in mutantClones.items():
#                         mutCloneSize.append({"week": week, "mutant_clone_size": get_num_of_cells(cloneAgents)})
#                 else:
#                     for cloneID, cloneAgents in clones.items():
#                         cloneSize.append({"week": week, "clone_size": get_num_of_cells(cloneAgents)})
#
#
#         totalNumOfClonesPerWeek.setdefault(week, 0)
#
#         fragmentedClonesPerWeek.setdefault(week, 0)
#         cloneClusteringPerWeek.setdefault(week, [])
#
#         totalNumOfClonesPerWeek[week] += len(clones)
#
#                 if 'fragmentation' in analysisToRun or 'cloneClustering' in analysisToRun:
#             for cloneID, cloneAgents in clones.items():
#                 cloneGraph = get_clone_graph(cloneAgents)
#                 if 'fragmentation' in analysisToRun:
#                     if (is_fragmented(cloneGraph)):
#                         fragmentedClonesPerWeek[week] += 1
#                 if 'cloneClustering' in analysisToRun:
#                     cloneClusteringPerWeek[week].append(nx.average_clustering(cloneGraph))
#
#         ##### Cell population analysis ########
#         if 'cellPopulations' in analysisToRun:
#             # for index, agent in agents.iterrows():
#             #     cellPopulations.setdefault(agent["cell-type"], [0] * numberOfweeks)
#             #     cellPopulations[agent["cell-type"]][week] += 1 / numOfSims
#
#             if induction_level>0:
#                 mutantPercentagePerWeek.setdefault(week,[])
#                 mutantPercentagePerWeek[week].append(get_mutant_percentage(agents))
#
#     if 'cellPopulations' in analysisToRun:
#         mutantPercentageFH = open(analysis_output+"dump_vars/mutantPercentagePerWeek", 'wb')
#         pickle.dump(mutantPercentagePerWeek, mutantPercentageFH)
#     if 'averageCloneSize' in analysisToRun:
#         totalMutCloneSizePerWeekFH = open(analysis_output+"dump_vars/totalMutCloneSizePerWeek", 'wb')
#         pickle.dump(totalMutCloneSizePerWeek, totalMutCloneSizePerWeekFH)
#         totalWTCloneSizePerWeekFH = open(analysis_output+"dump_vars/totalWTCloneSizePerWeek", 'wb')
#         pickle.dump(totalWTCloneSizePerWeek, totalWTCloneSizePerWeekFH)
#
#     ########################
#     ####    PLOTING     ####
#     ########################
#
#     #mutantPercentage = open(analysis_output+"dump_vars/mutantPercentagePerWeek.txt", 'rb')
#     #mutantPercentagePerWeek = pickle.load(mutantPercentage)
#
#     if 'cellPopulations' in analysisToRun:
#         #plot_avg_cell_population_per_week(cellPopulations, numberOfweeks)
#         plot_mutant_percentage_per_week(mutantPercentagePerWeek)
#         log(log_file, 'cellPopulations')
#
#     if 'averageCloneSize' in analysisToRun:
#         if induction_level > 0:
#             plotAvgCloneSizePerWeek(totalMutCloneSizePerWeek, "MUT")
#             plotAvgCloneSizePerWeek(totalWTCloneSizePerWeek, "WT")
#         else:
#             plotAvgCloneSizePerWeek(totalCloneSizePerWeek, "WT")
#         log(log_file, 'averageCloneSize')
#
#     if 'fragmentation' in analysisToRun:
#         plot_fragmentation_per_week(totalNumOfClonesPerWeek, fragmentedClonesPerWeek)
#         log(log_file, 'fragmentation')
#     if 'cloneClustering' in analysisToRun:
#         plot_clustering_per_week(cloneClusteringPerWeek)
#         log(log_file, 'cloneClustering')
#     if 'gridSegregation' in analysisToRun:
#         plot_distributions(chunkSizePerWeek, 'chunk_size')
#         log(log_file, 'gridSegregation_chunkSize')
#         plot_distributions(localRho, 'local_rho')
#         log(log_file, 'gridSegregation_localRho')
#     if 'cloneSizeDistribution' in analysisToRun:
#         if induction_level>0:
#             plot_distributions(mutCloneSize, 'mutant_clone_size')
#             plot_distributions(wtCloneSize, 'wt_clone_size')
#         else:
#             plot_distributions(cloneSize, 'clone_size')
#         log(log_file, 'cloneSizeDistribution')

main()

# TODO improve plot functions, add titles, axis names, legends, configure axis scale
# TODO pass multiple (optional) data structures to plot functions. Helpful for comparisons among different model versions

# sns.set()
# ax = sns.distplot(chunkLocalRhoPerWeek[0])
# ax = sns.distplot(chunkLocalRhoPerWeek[20])
# ax = sns.distplot(chunkLocalRhoPerWeek[40])
# ax = sns.distplot(chunkLocalRhoPerWeek[60])
# ax = sns.distplot(chunkLocalRhoPerWeek[80])
# plt.show()
# sys.exit()
# fig, ax = plt.subplots()
# n, bins, patches = ax.hist(chunkLocalRhoPerWeek[80], density=1, bins="auto")
# sigma=np.array(chunkLocalRhoPerWeek[80]).std()
# mu=np.array(chunkLocalRhoPerWeek[80]).mean()
# print(mu, sigma)
# y = mlab.normpdf(bins, mu,sigma)
# ax.plot(bins, y, 'r--')
# fig.tight_layout()
# plt.show()
